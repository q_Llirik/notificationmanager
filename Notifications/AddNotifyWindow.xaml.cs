﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notifications
{
    /// <summary>
    /// Логика взаимодействия для AddNotifyWindow.xaml
    /// </summary>
    public partial class AddNotifyWindow : Window
    {

        public AddNotifyWindow()
        {
            InitializeComponent();

            StaticClass.OpenedWindow.Add(2);

            var dt = DateTime.Now;
            dpDate.SelectedDate = dt;
            tbxHour.Text = dt.Hour + "";
            tbxMin.Text = dt.Minute + "";
            tbxSec.Text = "0";
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (dpDate.SelectedDate != null && tbxText.Text.Length != 0 && tbxHour.Text.Length != 0 && tbxMin.Text.Length != 0 && tbxSec.Text.Length != 0)
            {
                var dt = dpDate.SelectedDate;
                DBHelper.AddNewNotify(new Notify(tbxText.Text, new DateTime(dt.Value.Year, dt.Value.Month, dt.Value.Day, int.Parse(tbxHour.Text), int.Parse(tbxMin.Text), int.Parse(tbxSec.Text))));
                MessageBox.Show("Упоминание добавлено!", "Perfect", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Введите данные!", "Error", MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            StaticClass.OpenedWindow.Remove(2);
            if (!StaticClass.OpenedWindow.Contains(1))
                new MainWindow().Show();
        }

        private void CheckInput_KU(object sender, KeyEventArgs e)
        {
            int i;
            var tbx = (TextBox)sender;
            if (!int.TryParse(tbx.Text, out i) || e.Key == Key.Space)
            {
                try
                {
                    tbx.Text = tbx.Text.Remove(tbx.Text.Length-1);
                    tbx.Select(tbx.Text.Length, 0);
                }
                catch 
                {
                }
            }
        }

        private void DragDrop(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Close_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Minimized_Click(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}
