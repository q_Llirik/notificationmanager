﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications
{
    static class DBHelper
    {
        /// <summary>
        /// Выдаёт строку подключения.
        /// </summary>
        /// <returns>Строка подключения</returns>
        public static string GetConnectionString()
        {
            return "Data Source=.\\SQLEXPRESS;Initial Catalog=Notifications;" +
            "Integrated Security=true;";
        }

        public static List<Notify> SelectAllNotify()
        {
            List<Notify> list = new List<Notify>();
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            string query = "Select * From Notify";
            SqlCommand command = new SqlCommand(query, myConnection);
            var reader = command.ExecuteReader();
            while(reader.Read())
            {
                var nt = new Notify(reader.GetInt32(0), reader.GetString(1), reader.GetDateTime(2));
                list.Add(nt);
            }
            myConnection.Close();
            return list;
        }

        public static void AddNewNotify(Notify notify)
        {
            NonExecuteQuery("INSERT INTO Notify ( Title, Time) Values(" +
                "'" + notify.Title + "','" + notify.DateTime + "')");
        }

        /// <summary>
        /// Выполняет запрос к базе данных, который не нуждается в чтении ответа.
        /// </summary>
        /// <param name="query">Пользовательский запрос.</param>
        public static void NonExecuteQuery(string query)
        {
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            SqlCommand command = new SqlCommand(query, myConnection);
            command.ExecuteNonQuery();
            myConnection.Close();
        }
    }
}
