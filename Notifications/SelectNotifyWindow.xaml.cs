﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notifications
{
    /// <summary>
    /// Логика взаимодействия для SelectNotifyWindow.xaml
    /// </summary>
    public partial class SelectNotifyWindow : Window
    {
        public SelectNotifyWindow()
        {
            InitializeComponent();

            StaticClass.OpenedWindow.Add(3);

            txbTitle.Text = StaticClass.Title;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StaticClass.OpenedWindow.Remove(3);
            if (!StaticClass.OpenedWindow.Contains(1))
                new MainWindow().Show();
        }

        private void Minimized_Click(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Close_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void DragDrop(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
