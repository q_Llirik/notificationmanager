﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications
{
    class Notify
    {
        public int? ID { get; set; }
        public string Title { get; set; }
        public DateTime DateTime { get; set; }
        
        public Notify(int id, string title, DateTime dateTime)
        {
            ID = id;
            Title = title;
            DateTime = dateTime;
        }

        public Notify(string title, DateTime dateTime)
        {
            ID = null;
            Title = title;
            DateTime = dateTime;
        }
    }
}
