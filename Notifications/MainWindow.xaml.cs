﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Notifications
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Notify notify;
        private bool IsNeedToClosePopup = false;

        public MainWindow()
        {
            InitializeComponent();

            StaticClass.OpenedWindow.Add(1);

            if (!StaticClass.Isstarted)
            {
                SetIcon(new System.Windows.Forms.NotifyIcon());
                StaticClass.Isstarted = true;
            }

            notify = GetNeedNotify();
            if (notify != null)
                SetTimer(new DispatcherTimer());

            UpDateListView(lv, DBHelper.SelectAllNotify().Where(w => w.DateTime > DateTime.Now));
        }

        private void UpDateListView(ListView lv, IEnumerable<Notify> list)
        {
            foreach(var i in list)
            {
                lv.Items.Add(i);
            }
        }

        private void SetIcon(System.Windows.Forms.NotifyIcon icon)
        {
            icon.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.icon.GetHicon());
            icon.Visible = true;
            icon.ContextMenu = new System.Windows.Forms.ContextMenu(
                new System.Windows.Forms.MenuItem[] {
                    new System.Windows.Forms.MenuItem("Показать окно", delegate {
                        if (new MainWindow().Visibility == Visibility.Collapsed && new AddNotifyWindow().Visibility == Visibility.Collapsed && new SelectNotifyWindow().Visibility == Visibility.Collapsed)
                            this.Show();
                    }),
                    new System.Windows.Forms.MenuItem("Выход", delegate { Environment.Exit(0); })
                });
        }

        private void SetTimer(DispatcherTimer timer)
        {
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += delegate {
                var dt = DateTime.Now;
                if (dt.Date == notify.DateTime.Date && dt.Hour == notify.DateTime.Hour && dt.Minute == notify.DateTime.Minute && dt.Second == notify.DateTime.Second)
                {
                    SetTextToPopup(notify.Title);
                    pp.IsOpen = true;
                    System.Threading.Thread.Sleep(1000);
                    UpDateListView(lv, DBHelper.SelectAllNotify().Where(w => w.DateTime > DateTime.Now && w.ID != notify.ID));
                }
            };
            timer.Start();
        }

        private void SetTextToPopup(string text)
        {
            if (text.Length > 205)
                text = text.Substring(0, 205) + "...";

            txbTitle.Text = text;
        }

        private Notify GetNeedNotify()
        {
            Notify result = null;
            double k = -1000000000000000000;
            var dt = DateTime.Now;

            foreach(var i in DBHelper.SelectAllNotify())
            {
                var seconds = dt.Subtract(i.DateTime).TotalSeconds;
                if (seconds < 0 && seconds > k)
                {
                    k = seconds;
                    result = i;
                }
            }
            return result;
        }

        private void NewNotify_Click(object sender, RoutedEventArgs e)
        {
            new AddNotifyWindow().Show();
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StaticClass.OpenedWindow.Remove(1);
            e.Cancel = true;
            this.Hide();
        }

        private void DragDrop(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Close_Click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Minimized_Click(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void PopupOpen_Click(object sender, RoutedEventArgs e)
        {
            pp.IsOpen = true;
        }

        private void PopupClose_MLBD(object sender, MouseButtonEventArgs e)
        {
            IsNeedToClosePopup = true;
            pp.IsOpen = false;
        }

        private void Popup_Opened(object sender, EventArgs e)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += delegate {
                if (IsNeedToClosePopup)
                {
                    IsNeedToClosePopup = false;
                    txbTimer.Text = "21";
                    timer.Stop();
                }
                txbTimer.Text = Convert.ToInt32(txbTimer.Text) - 1 + "";
                var sec = Convert.ToInt32(txbTimer.Text);
                if (sec == 0)
                {
                    pp.IsOpen = false;
                    txbTimer.Text = "20";
                    timer.Stop();
                }
            };
            timer.Start();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (lv.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите напоминание в левой панели.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var selNot = (Notify)lv.SelectedItem;
            DBHelper.NonExecuteQuery("Delete From Notify where ID = " + selNot.ID);
            UpDateListView(lv, DBHelper.SelectAllNotify().Where(w=>w.DateTime > DateTime.Now));
            notify = GetNeedNotify();
            MessageBox.Show("Напоминание удалено.", "Perfect", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void OpenNotify_MLBD(object sender, MouseButtonEventArgs e)
        {
            StaticClass.Title = notify.Title;
            pp.IsOpen = false;
            new SelectNotifyWindow().Show();
        }
    }
}
